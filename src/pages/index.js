import React from "react"

import { Footer } from '../components/Footer';
import {Header} from '../components/Header';
export default function Home() {
  return <div>
    <Header/>
    <div className="main-content">
      Content
    </div>
    Hello world!
    <Footer/>
    </div>
}
