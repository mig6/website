const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/developer/website/.cache/dev-404-page.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/developer/website/src/pages/index.js"))),
  "component---src-pages-security-js": hot(preferDefault(require("/Users/developer/website/src/pages/security.js")))
}

